/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.hron.javaversion;

import java.util.Locale;

/**
 *
 * @author hron
 */
public class UserInfo {

    private String name;
    private String language;
    private String country;
    private String homeDirectory;

    private static final UserInfo INSTANCE = new UserInfo();

    public UserInfo() {
        try {
            name = System.getProperty("user.name");
            homeDirectory = System.getProperty("user.home");
        } catch (SecurityException sex) {
            name = "_duke";
            homeDirectory = "/home/_duke";

        }

        language = Locale.getDefault().getDisplayLanguage(Locale.ENGLISH);
        country = Locale.getDefault().getDisplayCountry(Locale.ENGLISH);

    }

    public String getCountry() {
        return country;
    }

    public String getHomeDirectory() {
        return homeDirectory;
    }

    public String getLanguage() {
        return language;
    }

    public String getName() {
        return name;
    }

    public static UserInfo getInstance() {
        return INSTANCE;
    }
}
