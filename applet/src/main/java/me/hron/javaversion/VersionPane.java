/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.hron.javaversion;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 *
 * @author hron
 */
public class VersionPane extends JPanel {

    public VersionPane() {
        this.setBorder(new TitledBorder("Java version informations"));
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JLabel javaInfo = new JLabel();
        JLabel osInfo = new JLabel();
        JLabel userInfo = new JLabel();

        javaInfo.setText(
                String.format("<html><p>Java(TM) Runtime Environment (%1$s_%2$s)</p><p>%3$s (%4$s, %5$s)</p><p>Vendor: %6$s</p></html>",
                    VMInfo.getInstance().getPlatformVersion(),
                    VMInfo.getInstance().getPlatformUpdate(),
                    VMInfo.getInstance().getVMName(),
                    VMInfo.getInstance().getVMVersion(),
                    VMInfo.getInstance().getVMInfo(),
                    VMInfo.getInstance().getVMVendor()
                )
        );
        osInfo.setText(
                String.format("Operating system: %1$s %2$s",
                    OSInfo.getInstance().getName(),
                    OSInfo.getInstance().getVersion()
                )
        );
        userInfo.setText(
                String.format("User: %1$s, speaking %2$s language at %3$s",
                UserInfo.getInstance().getName(),
                UserInfo.getInstance().getLanguage(),
                UserInfo.getInstance().getCountry()
                )
        );

        this.add(javaInfo);
        this.add(osInfo);
        this.add(userInfo);

    }

}
