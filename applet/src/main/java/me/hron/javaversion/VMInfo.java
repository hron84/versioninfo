/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.hron.javaversion;

/**
 *
 * @author hron
 */
public class VMInfo {

    private String platformVersion;
    private String platformUpdate;
    private String platformVendor;
    private String specificationName;
    private Double specificationVersion;
    private String specificationVendor;
    private String vmName;
    private String vmVersion;
    private String vmVendor;
    private String vmInfo;
    private static final VMInfo INSTANCE = new VMInfo();

    public VMInfo() {
        String ver = System.getProperty("java.version");
        String[] vparts = ver.split("_");

        platformVersion = vparts[0];

        if (vparts.length > 1) {
            platformUpdate = vparts[1];
        }

        specificationName = System.getProperty("java.specification.name");
        specificationVendor = System.getProperty("java.specification.vendor");
        try {
            specificationVersion = Double.parseDouble(System.getProperty("java.specification.version"));
        } catch (NumberFormatException ex) {
            specificationVersion = Double.parseDouble(platformVersion.substring(0, 3));
        }
        try {
            vmName = System.getProperty("java.vm.name");
            vmVersion = System.getProperty("java.vm.version");
            vmVendor = System.getProperty("java.vm.vendor");
            vmInfo = System.getProperty("java.vm.info");
        } catch (SecurityException sex) {
            vmName = "Unknown Java VM";
            vmVendor = "Unknown";
            vmInfo = "Unknown";
            vmVersion = platformVersion.substring(0, 3);
        }
    }

    public static VMInfo getInstance() {
        return INSTANCE;
    }

    public String getPlatformUpdate() {
        return platformUpdate;
    }

    public String getPlatformVendor() {
        return platformVendor;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public String getVMName() {
        return vmName;
    }

    public String getVMVendor() {
        return vmVendor;
    }

    public String getVMVersion() {
        return vmVersion;
    }

    public String getVMInfo() {
        return vmInfo;
    }

    public String getSpecificationName() {
        return specificationName;
    }

    public String getSpecificationVendor() {
        return specificationVendor;
    }

    public Double getSpecificationVersion() {
        return specificationVersion;
    }
}
