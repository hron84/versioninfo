/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.hron.javaversion;

import java.io.Serializable;

/**
 *
 * @author hron
 */
public class OSInfo implements Serializable {

    public enum Platform {

        LINUX,
        WINDOWS,
        MACOS,
        UNIX,
        OTHER
    }
    private String name;
    private String version;
    private Platform platform;
    private String architecture;
    private String platformVersion;
    private String platformVendor = "Unknown";
    private static final OSInfo INSTANCE = new OSInfo();

    public OSInfo() {
        String os = System.getProperty("os.name");
        String osver = System.getProperty("os.version");
        String[] osparts = os.split(" ");

        if (osparts[0].equalsIgnoreCase("linux")) {
            name = "GNU/Linux";
            platform = Platform.LINUX;
            version = osver;
            platformVersion = osver;
            platformVendor = "Linux";

        } else if (osparts[0].equalsIgnoreCase("windows")) {
            name = "Microsoft Windows";
            version = osparts[1];
            platform = Platform.WINDOWS;
            platformVersion = osver;
            platformVendor = "Microsoft";

        } else if (osparts[0].equalsIgnoreCase("mac")) {
            name = os;
            platform = Platform.MACOS;
            version = osver;
            platformVersion = osver;
            platformVendor = "Apple";
            // TODO Unix detection
        } else {
            name = os;
            platform = Platform.OTHER;
            version = osver;
            platformVersion = osver;
        }

        String arch = System.getProperty("os.arch");

        if (arch.equals("i386") || arch.equals("i486") || arch.equals("i586")
                || arch.equals("i686")) {
            architecture = "x86";
        } else {
            architecture = arch;
        }
    }

    public static OSInfo getInstance() {
        return INSTANCE;
    }

    public Platform getPlatform() {
        return platform;
    }

    public String getPlatformVendor() {
        return platformVendor;
    }

    public String getPlatformVersion() {
        return platformVersion;
    }

    public String getArchitecture() {
        return architecture;
    }

    public String getName() {
        return name;
    }

    public String getVersion() {
        return version;
    }
}
