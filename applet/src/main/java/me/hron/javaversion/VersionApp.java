/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package me.hron.javaversion;

import javax.swing.JFrame;

/**
 *
 * @author hron
 */
public class VersionApp extends JFrame {
    public static void main(String[] args) {
        JFrame frm = new VersionApp();
        frm.setVisible(true);
    }

    public VersionApp() {
        super("Java version info test");

        this.add(new VersionPane());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
    }


}
